﻿namespace Meta
{
    public class Model
    {
        public string GivenName { get; set; }
        
        public string FamilyName { get; set; }
    }
}